﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class NodeBuilder : MonoBehaviour {
	
	private BoxCollider currCollider;
	private Node currentNode;
	private List<Node> nodeList = new List<Node>();
	private int nodeCount = 0;
	private bool building = false;
	private bool loading = false;
	private bool saving = false;
	
	public Monster monster;
	public Player player;
	public DataHandler handler;
	public int xNodesPerRow = 70;
	public int zNodesPerRow = 35;
	// Use this for initialization
	void Start() {
		currCollider = gameObject.AddComponent<BoxCollider>();
	}
	
	// Update is called once per frame
	void Update() {
		if(Input.GetKey(KeyCode.R) && !building) {
			building = true;
			StartCoroutine(BuildNodes(-97.19f, 192.53f, -55.34f, 98.51f));
		}
		if(nodeCount < xNodesPerRow * zNodesPerRow && building)
			//Debug.Log ("Total nodes: "+nodeCount+" Passable nodes: "+nodeList.Count);
		if(Input.GetKey(KeyCode.B) && !saving) {
			saving = true;
			handler.BuildNodeFile(nodeList);
		}
		if(Input.GetKey(KeyCode.L) && !loading){
			loading = true;
			handler.LoadNodeFile();
		}
	}
	
	IEnumerator BuildNodes(float xstart, float xsize, float zstart, float zsize) {
		currCollider.size = new Vector3(xsize / xNodesPerRow, 20f, zsize / zNodesPerRow);
		for(int x = 0; x < xNodesPerRow; x++) {
			for(int z = 0; z < zNodesPerRow; z++) {
				gameObject.rigidbody.velocity = new Vector3(0, 0, 0);
				gameObject.transform.position = new Vector3(xstart + xsize / xNodesPerRow * x, 1f, zstart + zsize / zNodesPerRow * z);
				currentNode = new Node(x, z, gameObject.transform.position.x, gameObject.transform.position.z);
				nodeList.Add(currentNode);
				nodeCount++;
				yield return new WaitForSeconds(Time.deltaTime * 2);
			}
		}

		//The entrance box
		float xstart2 = -16.28f;
		float xsize2 = 33.36f;
		float zstart2 = 43.17f;
		float zsize2 = 19.47f;
		int xNodesPerRow2 = 12;
		int zNodesPerRow2 = 7;

		currCollider.size = new Vector3(xsize2 / xNodesPerRow2, 20f, zsize2 / zNodesPerRow2);
		for(int x = 0; x < xNodesPerRow2; x++) {
			for(int z = 0; z < zNodesPerRow2; z++) {
				gameObject.transform.position = new Vector3(xstart2 + xsize2 / xNodesPerRow2 * x, 1f, zstart2 + zsize2 / zNodesPerRow2 * z);
				currentNode = new Node(x + xNodesPerRow, z + zNodesPerRow, gameObject.transform.position.x, gameObject.transform.position.z);
				currentNode.Entrance = true;
				nodeList.Add(currentNode);
				nodeCount++;
				yield return new WaitForSeconds(Time.deltaTime * 2);
			}
		}

		for(int i = 0; i < nodeList.Count; i++) {
			for(int j = i; j < nodeList.Count; j++) {
				if(Mathf.Abs(nodeList[i].XID - nodeList[j].XID) <= 1 && 
				   Mathf.Abs(nodeList[i].ZID - nodeList[j].ZID) <= 1
				   && i != j) {
					nodeList[i].Neighbors.Add(nodeList[j]);
					nodeList[j].Neighbors.Add(nodeList[i]);
				}
			}
			if(nodeList[i].Neighbors.Count == 0)
				nodeList.RemoveAt(i);
			else
				////Debug.Log("Total neighbors: "+nodeList[i].Neighbors.Count);
			yield return new WaitForSeconds(Time.deltaTime * 2);
		}
		player.NodeList = nodeList;
		monster.NodeList = nodeList;
		////Debug.Log("Node Building complete!");
		StartCoroutine(AddPortalLinks(xsize, zsize, xsize2, zsize2));
		yield break;
	}
	
	IEnumerator AddPortalLinks(float xsize, float zsize, float xsize2, float zsize2) {
		////Debug.Log("Adding portal links!");
		GameObject[] portals = GameObject.FindGameObjectsWithTag("Mirror");
		foreach(Node node in nodeList) {
			foreach(GameObject portal in portals) {
				if(node.Distance(portal.transform.position.x, portal.transform.position.z) <= 
				  (node.Entrance ? Common.Hyp(xsize2, 0, zsize2, 0) : Common.Hyp(xsize, 0, zsize, 0)) && !node.AtPortal) {
					////Debug.Log("First node found for portal!");
					Node connectNode = null;
					var cameraArray = portal.transform.Cast<Transform>().Where(c=>c.gameObject.tag == "PortalCam").ToArray();
					if(cameraArray[0].gameObject != null) {
						foreach(Node node2 in nodeList) {
							if(connectNode == null) {
								connectNode = node2;
							}
							else if(node2.Distance(cameraArray[0].position.x, cameraArray[0].position.z)
							        < connectNode.Distance(cameraArray[0].position.x, cameraArray[0].position.z)) {
								connectNode = node2;
							}
						}
					}
					if(connectNode != null && node != connectNode && !connectNode.AtPortal) {
						node.AtPortal = true;
						connectNode.AtPortal = true;
						node.Neighbors.Add(connectNode);
						connectNode.Neighbors.Add(node);
						//Debug.Log("Portal nodes linked!");
					}
				}
			}
		}
		//Debug.Log("fin");
		yield break;
	}
	
	void OnCollisionEnter(Collision collision) {
		if(collision.collider.tag == "Wall") {
			nodeList.Remove(currentNode);
		}
	}
	
	public void LoadNodes(List<Node> loadList) {
		player.NodeList = loadList;
		monster.NodeList = loadList;
		building = true;
	}

	private Node FindClosestNode(float x, float z) {
		Node currentNode = null;
		foreach(Node node in nodeList) {
			if(currentNode == null) {
				currentNode = node;
			}
			else if(Common.Hyp(node.XCenter, x, node.ZCenter, z) < Common.Hyp(currentNode.XCenter, x, currentNode.ZCenter, z)) {
					currentNode = node;
			}
		}
		return currentNode;
	}
}



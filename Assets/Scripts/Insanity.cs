﻿using UnityEngine;
using System.Collections;

public class Insanity : MonoBehaviour {

	public float CurrentValue { get; set; }
	public float startValue;
	public float minValue;
	public float maxValue;
	private float stepValue = 1f;
	public Common common;
	private float monsterCooldown = 1;
	public Player player;
	public Monster monster;
	private float baseCooldown = 3;
	private float baseIncrease = 0.1f;

	Material monster1;
	Material monster2;

	// Use this for initialization
	void Start() {
		CurrentValue = startValue;
	}
	
	// Update is called once per frame
	void Update() {
		GameObject full = transform.Find("Full").gameObject;
		GameObject empty = transform.Find("Empty").gameObject;
		float widthMultiplier =  (float)empty.guiTexture.pixelInset.width / maxValue;
		full.guiTexture.pixelInset = new Rect(0, 0, CurrentValue * widthMultiplier, empty.guiTexture.pixelInset.height);
		empty.guiTexture.pixelInset = new Rect(0, 0, empty.guiTexture.pixelInset.width, empty.guiTexture.pixelInset.height);
		monsterCooldown -= Time.deltaTime;
		if(monsterCooldown <= 0 && common.CheckVisible(monster.gameObject, "Monster", player.gameObject, 50) && CurrentValue + stepValue <= maxValue) {
			CurrentValue += stepValue;
			monsterCooldown = 1;
		}

		foreach(Material matt in monster.renderer.materials)
		matt.SetFloat ("_GlowAmount", (float)10.0*Mathf.Sin ((float)0.5*CurrentValue*Time.time));


		if(baseCooldown <= 0 && CurrentValue + baseIncrease <= maxValue) {
			baseCooldown = 3;
			CurrentValue += baseIncrease;
		}

	}
}

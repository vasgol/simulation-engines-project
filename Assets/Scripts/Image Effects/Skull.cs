﻿using UnityEngine;
using System.Collections;

public class Skull : MonoBehaviour {
	public Insanity insanity;
	// Use this for initialization
	void Start () {
		renderer.material.SetFloat ("_GlowAmount", (float)0.0);
	}
	
	// Update is called once per frame
	void Update () {
		renderer.material.SetFloat ("_GlowAmount", (float)10.0*Mathf.Sin ((float)0.5*insanity.CurrentValue*Time.time));

	}
}

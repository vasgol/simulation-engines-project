﻿// Shader providing glow on a specific object (3 passes)
Shader "Custom/Glow" {
	
     Properties {
		_Color ("Color", Color) = (1.0,1.0,1.0,0.4)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_GlowAmount ("Glow Amount", Range(0,5)) = 0
	 }
			
	 CGINCLUDE
	 #include "UnityCG.cginc"
	
	 uniform sampler2D _MainTex;   
     uniform float4 _Color; 
 	 uniform float _GlowAmount;
 		 
	 // based input structs
	 struct vertexOutput{
		float4 pos : SV_POSITION; // DirectX 11 support
		float2 uv : TEXCOORD0;
	 };	
	 
	  // based input structs
	 struct vertexOutputhb{
		float4 pos : SV_POSITION; // DirectX 11 support
		float2 uv : TEXCOORD0;
	 };	
	 
	   // based input structs
	 struct vertexOutputvb{
		float4 pos : SV_POSITION; // DirectX 11 support
		float2 uv : TEXCOORD0;
	 };	
	 
	// FIRST PASS : Isolates glow sources (RGB*alpha) masked by alpha channel						
	// vertex function
	vertexOutput vert(appdata_img v){
	
		vertexOutput o;
		
		
		o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
		o.uv = v.texcoord.xy;
	
	
		return o;
	}
	
	// fragment function
	half4 frag(vertexOutput i) : COLOR
	{					    
	// Assign color of the texture
		float4 col = tex2D(_MainTex, i.uv);
		
		
		return col*_GlowAmount*(1-col.a);
		
	}	
	
	// SECOND PASS: Horizontal Gaussian Blurring
	// vertex function
	vertexOutputhb verthb(appdata_img v){
	
		vertexOutputhb o;
		
		
		o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
		o.uv = v.texcoord.xy;

		return o;
	}
	
	// Horizontal Blurring fragment function
	half4 fraghb(vertexOutputhb i) : COLOR
	{
		// 9 samples for blurring
		float weights[9] = {0.0125949685786212, 0.0513831777608629, 0.1359278107392780, 0.2333084327472980, 0.1335712203478790, 0.2333084327472980, 0.1359278107392780, 0.0513831777608629, 0.0125949685786212};
	    float samples[9] = {-0.07302940716,-0.0535180578,-0.03403984807,-0.01458429517, 0.0 ,0.01458429517,0.03403984807,  0.0535180578, 0.07302940716};	
	    // Horizontal 
	    float2 direction = i.uv - (1.0,0.0);
	    
	// Assign color of the texture
		float4 col = 0.0;
		float4 sum = col;


		// take 9 additional blur samples bilinearly horizontally
	    for (int pix = 0; pix < 9; pix++)
		  sum += tex2D( _MainTex, i.uv + float2(samples[pix],0.0))*weights[pix]*_GlowAmount;

		
	    return sum;
	
	}	
		
	// SECOND PASS: Vertical Gaussian Blurring
	vertexOutputvb vertvb(appdata_img v){
	
		vertexOutputvb o;
		
		
		o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
		o.uv = v.texcoord.xy;
	
	
		return o;
	}
	
	// Vertical Blurring fragment function
	half4 fragvb(vertexOutputvb i) : COLOR
	{
		// 9 samples for blurring
		float weights[9] = {0.0125949685786212, 0.0513831777608629, 0.1359278107392780, 0.2333084327472980, 0.1335712203478790, 0.2333084327472980, 0.1359278107392780, 0.0513831777608629, 0.0125949685786212};
	    float samples[9] = {-0.07302940716,-0.0535180578,-0.03403984807,-0.01458429517, 0.0 ,0.01458429517,0.03403984807,  0.0535180578, 0.07302940716};	
	    // Horizontal 
	    float2 direction = i.uv - (1.0,0.0);
	    
	// Assign color of the texture
		float4 col = 0.0;
		float4 sum = col;


		// take 9 additional blur samples bilinearly horizontally
	    for (int pix = 0; pix < 9; pix++)
		  sum += tex2D( _MainTex, i.uv + float2(0.0,samples[pix]))*weights[pix]*_GlowAmount;
		
		return sum;
	
	}	

	ENDCG
		
		SubShader {
		tags {"Queue" = "Overlay" "RenderType" = "Opaque"}

		
		// Isolate Glow sources of the object
		pass {
		   
			CGPROGRAM
			#pragma vertex verthb
			#pragma fragment fraghb
			#pragma target 3.0
			ENDCG
		}
		
		// Horizontal Blurring alpha blend 
		pass {
		    Blend SrcAlpha OneMinusSrcAlpha
			CGPROGRAM
			#pragma vertex vertvb
			#pragma fragment fragvb
			#pragma target 3.0
			ENDCG
		}
		
		// Vertical Blurring alpha blend
		pass {
	    	Blend SrcAlpha OneMinusSrcAlpha 
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			ENDCG
		}
		
		// Alpha blend, blurred glow texture with original scene texture
		Pass {
			Blend SrcAlpha OneMinusSrcAlpha 
			Fog { Mode off }
			Lighting Off
			
			SetTexture [_MainTex] {
				Combine texture
			}
		}
	}	
}

﻿// Screen Distortion Shader
Shader "Custom/Distortion" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Amount ("Distortion Amount", Range(0,10)) = 10
	}
	
	SubShader {
		Pass {
			ZTest Always Cull Off ZWrite Off
		    
			CGPROGRAM		
			// pragmas
			#pragma vertex vert
			#pragma fragment frag
			#pragma exclude_renderers flash
			#include "UnityCG.cginc"
			
			sampler2D _MainTex;
			float _Amount;
			
			// based input structs
			struct vertexOutput{
				float4 pos : SV_POSITION; // DirectX 11 support
				float2 uv : TEXCOORD0;
			};
			
			// vertex function
			vertexOutput vert(appdata_img v){
			
				vertexOutput o;
         	    
	            o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
	            
	            //_Amount*100*sin((2*3.14*unity_DeltaTime)/1000);
	            
	          
	            o.uv =  v.texcoord.xy; 

	            return o;
			}
			
			// fragment function
			half4 frag(vertexOutput i) : COLOR
			{
				float2 dis = i.uv;
				
				// Distorting texture UV
				dis.x += _Amount*unity_DeltaTime;
				
    			// Assign color of the texture
   				float4 col = tex2D(_MainTex, dis);
   				
   				if(col.x<0.5)
   				 col.x+=0.2;
   							 
   			
   				return col;
	
			}	
			ENDCG
		}
	}
			
	
}

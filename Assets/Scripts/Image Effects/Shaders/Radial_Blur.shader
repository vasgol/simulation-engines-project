﻿// Radial Blur Shader
Shader "Custom/Radial_Blur" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Amount ("Blur Amount", Range(0,10)) = 1
	}
			
	SubShader {
		Pass {
			ZTest Always Cull Off ZWrite Off
		    
			CGPROGRAM		
			// pragmas
			#pragma vertex vert
			#pragma fragment frag
			#pragma exclude_renderers flash
			#include "UnityCG.cginc"
			
			sampler2D _MainTex;
			float _Amount;
			
			// based input structs
			struct vertexOutput{
				float4 pos : SV_POSITION; // DirectX 11 support
				float2 uv : TEXCOORD0;
			};
			
			// vertex function
			vertexOutput vert(appdata_img v){
			
				vertexOutput o;
				
				
				o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.texcoord.xy;
			
			
				return o;
			}
			
			// fragment function
			half4 frag(vertexOutput i) : COLOR
			{
				// 10 samples for blurring
				float samples[10] = {-0.08,-0.05,-0.03,-0.02,-0.01,0.01,0.02,0.03,0.05,0.08};	
			    // 0.5,0.5 is the center of the screen
			    // so substracting uv from it will result in
			    // a vector pointing to the middle of the screen
				float2 direction = 0.5 - i.uv;
				// calculate the distance to the center of the screen
    			float distance = sqrt(direction.x*direction.x + direction.y*direction.y); 
    			// normalize direction vector
    			direction = normalize(direction);
    			// Assign color of the texture
   				float4 col = tex2D(_MainTex, i.uv);
   				float4 sum = col;
   				// take 10 additional blur samples in the direction towards
			    // the center of the screen
			    for (int pix = 0; pix < 10; pix++)
			    {
			       sum += tex2D( _MainTex, i.uv + direction * samples[pix]);
			    }
			    // Average value of samples
			    sum /= 11;
			    // weighten the blur effect with the distance to the
			    // center of the screen ( further out is blurred more)
			    float t = distance * _Amount;
 			    // Value of weight between 0-1 
			    t = saturate(t);
			 
			    //Blend the original color with the averaged pixels
			    return lerp(col, sum, t);
	
			}	
			ENDCG
		}
	}
}
﻿using UnityEngine;
using System.Collections;

// Simulates Render Texture - Parent Class to all post-processing scripts
// Basically stores screen pixels, to a texture using ReadPixels
public class RenderTexture : MonoBehaviour{

	public static Texture2D renderTex; //Holds 2D rendered texture from screen
	private static Rect rect;		   // Holds screen Quad rectangle

	public static int width;
	public static int height;

	// Start -> Assign renderTex and rect
	protected virtual void Start(){

		renderTex= new Texture2D(width, height, TextureFormat.RGB24, false);
		rect = new Rect (0, 0, width, height);
	}

	// Drawing a FullScreenQuad for the post processing effects
	public static void FullScreenQuad(Material mat)
	{
		GL.PushMatrix ();
		// for each pass in the material (one in this case)
		for (int i = 0; i < mat.passCount; ++i) {

			// activate pass
			mat.SetPass (i);

			GL.LoadOrtho ();
			// draw a quad
			GL.Begin( GL.QUADS );
			GL.TexCoord2( 0, 0 ); GL.Vertex3( 0, 0, 0.1f );
			GL.TexCoord2( 1, 0 ); GL.Vertex3( 1, 0, 0.1f );
			GL.TexCoord2( 1, 1 ); GL.Vertex3( 1, 1, 0.1f );
			GL.TexCoord2( 0, 1 ); GL.Vertex3( 0, 1, 0.1f );
			GL.End();
		}
		GL.PopMatrix ();
	}
	

	// 1. Resize render texture to screen size (in case of changing window)
	// 2. ReadPixels storing screen pixels to the texture
	// 3. Applying the texture (slow)
	protected virtual void OnPostRender(){

		renderTex.Resize(width, height, TextureFormat.RGB24, false);
		renderTex.ReadPixels(rect, 0, 0);
		renderTex.Apply();

	}
}




































































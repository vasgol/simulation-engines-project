﻿/* Main Script of the Graphics Module invoking shaders implementing the 
 * specific graphics effects (Radial Blurring, Distortion, Glow)
 * Child of rendertexture script, which renders a texture to the screen
 */
using UnityEngine;
using System.Collections;

// Graphics Module inheriting from RenderTexture
public class Graphics : RenderTexture {

	// Post-Processing Materials
	private static Material rb;	  
	private static Material dist;
	private static Material mir;

	// Shaders
	public Shader rblurShader;  // Radial Blur Shader
	public Shader distShader;	// Screen distortion shader
	public Shader mirShader;	// Mirror shader

	
	// Amount of radial blurring
	private float rbamount=0.0f;
	// Max radial blurring
	private float threshold=10.0f;

	// Amount of Screen Distortion
	private float distamount = 0.0f;

	// Mirror visible 
	private bool mirvisible = false;

	// Initialization of renderingtexture function and materials for graphic effects
	protected override void Start () {

		RenderTexture.width = Screen.width;
		RenderTexture.height = Screen.height;
		
		base.Start ();

		// Radial blur material
		rb = new Material(rblurShader);
		// Distortion material
		dist = new Material(distShader);

	}

	// Radial Blurring 
	public void radial_blurring(bool blurring, bool activated)
	{
		if(activated)
		{
			if(blurring == true)
			{
				rbamount += 0.05f;

				if (rbamount >= threshold)
					rbamount = threshold;
			}
			else 
				rbamount -= 0.05f;

			if (rbamount < 0.0f)
				rbamount = 0.0f;
		}
		else
			rbamount = 0.0f;

		rb.mainTexture = RenderTexture.renderTex;
		rb.SetTexture ("_MainTex", rb.mainTexture);
		rb.SetFloat ("_Amount", rbamount);
	}

	// Render texture to mirror from portal camera
	public void portal_mirror(bool mirvis, GameObject mirror)
	{
		mirvisible = mirvis;
		if (mirvisible) {
			mirror.renderer.enabled = true;
			mirror.renderer.material.mainTexture = RenderTexture.renderTex;
			mirror.renderer.material.SetTexture ("_MainTex", mirror.renderer.material.mainTexture);
				
		} 
		else
			mirror.renderer.enabled = false;

	}

	// Screen Distortion effect
	public void distortion(bool col)
	{

		if(col)
		{
			distamount = 10.0f;
			dist.mainTexture = RenderTexture.renderTex;
			dist.SetTexture ("_MainTex", dist.mainTexture);
			dist.SetFloat ("_Amount", distamount);
		}
		else
			distamount -= 10f;

		if(distamount < 0.0f)
			distamount = 0.0f;
	}


	// After rendering objects, apply post processing effects
	protected override void OnPostRender () {


		RenderTexture.width = Screen.width;
		RenderTexture.height = Screen.height;

		// Render radial blurring texture only if amount is greater than 0
		if (rbamount>0.0f) {
			// This is the bottomneck of rendertexture so it is invoked ONLY if necessary
			// Invoking parent onPostRender in order to read texture and apply it to 
			// RenderTexture 
			base.OnPostRender ();
			// Drawing full scren Quad with the post processing effect
			RenderTexture.FullScreenQuad (rb);
		}

		// Render on mirror portal only if it is visible from the camera
		if(mirvisible)
			// Applying texture to the mirror's surface
			base.OnPostRender ();
		// Render distortion effect when player is collided with monster
		if(distamount>0.0f){
			// Apply texture
			base.OnPostRender();
			// Draw full screen quad
			RenderTexture.FullScreenQuad(dist);
		}	
	}
}

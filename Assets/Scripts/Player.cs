﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Player : MonoBehaviour {

	public Camera camera;
	public float topSpeed;
	private bool buttonPressed = false;
	private bool jumping = false;
	public List<Node> NodeList { set; get; }
	public Node CurrentNode { set; get; }
	private float myTimer = 20.0f;
	private bool  ptime=false;
	public float sensitivityX = 2f;
	
	private float minimumX = -360f;
	private float maximumX = 360f;
	
	float rotationX = 0F;
	
	Quaternion originalRotation;

	void Start() {
		// Make the rigid body not change rotation
		if (rigidbody)
			rigidbody.freezeRotation = true;
		originalRotation = transform.localRotation;
	}

	void Update() {

	}
	
	public static float ClampAngle (float angle, float min, float max)
	{
		if (angle < -360f)
			angle += 360f;
		if (angle > 360f)
			angle -= 360f;
		return Mathf.Clamp (angle, min, max);
	}
	
	// Update is called once per frame
	void FixedUpdate() {

		// Toggle Full Screen Mode
		if (Input.GetKeyDown(KeyCode.F))
			Screen.fullScreen = !Screen.fullScreen;

		buttonPressed = false;

		rotationX += Input.GetAxis("Mouse X") * sensitivityX;
		rotationX = ClampAngle (rotationX, minimumX, maximumX);
		
		Quaternion xQuaternion = Quaternion.AngleAxis (rotationX, Vector3.up);
		transform.localRotation = originalRotation * xQuaternion;

		if(Input.GetKeyDown(KeyCode.Space) && !jumping) {
			rigidbody.velocity += transform.up * 7;
			jumping = true;
		}
		if(Input.GetKey(KeyCode.W) && !jumping){
			rigidbody.velocity += transform.forward * 3;
			buttonPressed = true;
		}
		if(Input.GetKey(KeyCode.A) && !jumping){
			rigidbody.velocity += -transform.right * 3;
			buttonPressed = true;
		}
		if(Input.GetKey(KeyCode.S) && !jumping){
			rigidbody.velocity += -transform.forward * 3;
			buttonPressed = true;
		}
		if(Input.GetKey(KeyCode.D) && !jumping){
			rigidbody.velocity += transform.right * 3;
			buttonPressed = true;
		}
		if(rigidbody.velocity.magnitude > topSpeed) {
			float yvel = rigidbody.velocity.y;
			rigidbody.velocity = Vector3.Scale(rigidbody.velocity, new Vector3(1, 0, 1));
			rigidbody.velocity = rigidbody.velocity.normalized * topSpeed;
			rigidbody.velocity += new Vector3(0, yvel, 0);
		}
		if(!buttonPressed && !jumping)
			rigidbody.velocity = new Vector3(0,0,0);

		myTimer -= Time.deltaTime; 
		if (myTimer <= 1.0) 
			ptime = true;
	}

	void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.tag == "Floor") {
				jumping = false;
		}
		if (collision.gameObject.tag == "Mirror") {
			var cameraArray = collision.gameObject.transform.Cast<Transform>().Where(c=>c.gameObject.tag == "PortalCam").ToArray();
			transform.position = new Vector3(cameraArray[0].transform.position.x, transform.position.y, cameraArray[0].transform.position.z);
			rotationX = cameraArray[0].transform.localRotation.y;
		}
	}

	public void FindClosestNode() {
		foreach(Node node in NodeList) {
			if(CurrentNode == null) {
				CurrentNode = node;
			}
			else{
				if(Mathf.Sqrt(Mathf.Pow(Mathf.Abs(node.XCenter - transform.position.x), 2)
				              + Mathf.Pow(Mathf.Abs(node.ZCenter - transform.position.z), 2)) < 
				   Mathf.Sqrt(Mathf.Pow(Mathf.Abs(CurrentNode.XCenter - transform.position.x), 2)
				           + Mathf.Pow(Mathf.Abs(CurrentNode.ZCenter - transform.position.z), 2))) {
					CurrentNode = node;
				}
			}
		}
	}

}

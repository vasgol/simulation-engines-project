﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Main Camera
public class Main_Camera : MonoBehaviour{
	
	private GameObject monster;
	private GameObject[] mirror;
	private GameObject player;
	private Common common;
	
	private Audio audio;
	private Graphics graphics;
	
	private bool monstervisible = false;
	private bool mirmonstervisible = false;
	private bool monstercollided = false;
	
	private bool playerVisible = false;
	private bool mirPlayerVisible = false;

	public bool blurEnabled = false;
	public bool soundEnabled = false;
	public bool audioEnabled = false;

	private List<int> pltomir = new List<int>();
	private List<int> motomir = new List<int>();

	void Start() {
		
		monster = GameObject.FindGameObjectWithTag ("Monster");
		mirror = GameObject.FindGameObjectsWithTag ("Mirror");

		player = GameObject.Find ("Player");
		common = (Common) GameObject.FindGameObjectWithTag ("Common").GetComponent("Common");
		
		audio = (Audio) this.GetComponent ("Audio");
		graphics = (Graphics)this.GetComponent("Graphics");


	}
	
	void Update()
	{ 
		if(Input.GetKeyDown(KeyCode.I))
		   blurEnabled = !blurEnabled;
		if(Input.GetKeyDown(KeyCode.O))
		   soundEnabled = !soundEnabled;
		if(Input.GetKeyDown(KeyCode.P))
		   audioEnabled = !audioEnabled;
		 

		//Raycast from player to monster
		monstervisible = common.CheckVisible (monster, "Monster", this.gameObject, 50);
		//Raycast from monster to player
		playerVisible = common.CheckVisible (player, "Player", monster, 50);

		for(int j = 0; j < mirror.Length ;j++){

			if(common.CheckVisible (mirror[j], "Mirror", this.gameObject, 50)==true)
				pltomir.Add(j);

			if(common.CheckVisible (mirror[j], "Mirror", monster, 50)==true)
				motomir.Add(j);

		}

		if(soundEnabled)
		{
			audio.monsterSeesPlayerAudio(playerVisible, false, mirror[0].transform.GetChild (0).gameObject, mirror[0]);
			audio.playerSeesMonsterAudio (monstervisible, false, mirror[0].transform.GetChild (0).gameObject, mirror[0]);
		}

		foreach (int k in motomir)
		{
			if(soundEnabled) 
			{
				mirPlayerVisible = common.CheckVisible (player, "Player", mirror[k].transform.GetChild (0).gameObject, 50);
				audio.monsterSeesPlayerAudio (playerVisible, mirPlayerVisible, mirror[k].transform.GetChild (0).gameObject, mirror[k]);
			}
		}
		mirmonstervisible = false;
		foreach (int i in pltomir)
		{
			if(blurEnabled)
				mirmonstervisible = common.CheckVisible (monster, "Monster", mirror[i].transform.GetChild (0).gameObject, 50);
		
			if(soundEnabled)
			{	
				mirmonstervisible = common.CheckVisible (monster, "Monster", mirror[i].transform.GetChild (0).gameObject, 50);
				audio.playerSeesMonsterAudio (monstervisible, mirmonstervisible, mirror[i].transform.GetChild (0).gameObject, mirror[i]);
			}
		}

		graphics.radial_blurring ((monstervisible || mirmonstervisible),blurEnabled);
		monstercollided = common.CheckRange (monster, "Monster", this.gameObject, 1);
		graphics.distortion (monstercollided);

		if(!audioEnabled)
			this.gameObject.audio.volume = 0.0f;
		if(audioEnabled)
			this.gameObject.audio.volume = 0.102f;

		pltomir = new List<int>();
		motomir = new List<int>();
	} 
}

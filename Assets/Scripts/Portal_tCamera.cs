﻿using UnityEngine;
using System.Collections;

public class Portal_tCamera : MonoBehaviour {

	private GameObject maincam;
	private	GameObject mirror;
	private GameObject monster;
	private Graphics graphics;
	private Common common;
	private string parentName;
	private bool mirrorvisible = false;
	private double myTimer;
	private bool ptime = false;
	
	
	void Start() {
		int nportal = 0;
		parentName = this.transform.parent.name;
		mirror = GameObject.Find (parentName);
		mirror.collider.enabled = false;

		//take the index of the portal 
		nportal = (int)(parentName [8]) - 48;
		if (nportal % 2 == 1)
						nportal -= 1;
		nportal = nportal * 10;

		//the timer will be 20 plus the portals index*10
		myTimer = 20.0 + nportal;

		maincam = GameObject.FindGameObjectWithTag ("Camera");
		monster = GameObject.FindGameObjectWithTag ("Monster");
		graphics = (Graphics)this.GetComponent("Graphics");
		common = (Common) GameObject.FindGameObjectWithTag ("Common").GetComponent("Common");
	}
	
	void Update()
	{
		myTimer -= Time.deltaTime;

		if (myTimer <= 0.0)
			mirror.collider.enabled = true;

		if(myTimer <= 0.0 && common.CheckVisible (mirror, "Mirror", maincam.gameObject, 50)){
			mirrorvisible = true;
		}
		graphics.portal_mirror (mirrorvisible, mirror);
		
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Monster : MonoBehaviour {

	public Player player;
	public List<Node> NodeList { set; get; }
	public Insanity insanity;
	public Common common;
	private bool searching = false;
	private Node currentNode;
	private List<Node> currentPath = new List<Node>();
	private float behaviorCooldown = 5;
	private float monsterAngle;
	private float playerAngle;
	enum Behavior { SUPERAGGRESSIVE, AGGRESSIVE, PASSIVE, STALKING };
	private Behavior behavior = Behavior.PASSIVE;

	private bool moveTarget = false;
	private bool portalTarget = false;
	private bool cameraTarget = false;
	private GameObject currPortal = null;
	private GameObject currCamera = null;
	private float targetX = 0.0f;
	private float targetZ = 0.0f;

	//Maybe the speed can depend on the insanity?
	public float speed = 0.05f;
	private float searchCooldown = 0.0f;

	// Use this for initialization
	void Start() {

	}
	
	// Update is called once per frame
	void Update() {
		if(Input.GetKey(KeyCode.F) && !searching) {
			searching = true;
			FindClosestNode();
			player.FindClosestNode();
			StartCoroutine(FindPath(player.CurrentNode));
		}
		behaviorCooldown -= Time.deltaTime;
		if(behaviorCooldown < 0) {
			behaviorCooldown = 30;
			NewBehavior();
		}
		if(NodeList != null) {
			if(moveTarget) {
				if(ProximityCheck()) {
					moveTarget = false;
					if(portalTarget) {
						//Debug.Log("Monster moved through portal!");
						var cameraArray = currPortal.transform.Cast<Transform>().Where(c=>c.gameObject.tag == "PortalCam").ToArray();
						transform.position = new Vector3(cameraArray[0].transform.position.x, transform.position.y, cameraArray[0].transform.position.z);
						portalTarget = false;
					}
					if(cameraTarget) {
						//Debug.Log("Monster moved through portal in reverse!");
						transform.position = new Vector3(currCamera.transform.parent.position.x, transform.position.y, currCamera.transform.parent.position.z);
						cameraTarget = false;
					}
				}
				else {
					float xdiff = targetX - transform.position.x;
					float zdiff = targetZ - transform.position.z;
					transform.position = new Vector3(transform.position.x + (xdiff * (speed / 2)), transform.position.y, transform.position.z + (zdiff * (speed / 2)));
				}
			}
			else {
				MoveOrder();
			}
		}
	}

	private void FindClosestNode() {
		foreach(Node node in NodeList) {
			if(currentNode == null) {
				currentNode = node;
			}
			else{
				if(Common.Hyp(node.XCenter, transform.position.x, node.ZCenter, transform.position.z) < 
				   Common.Hyp(currentNode.XCenter, transform.position.x, currentNode.ZCenter, transform.position.z)) {
					currentNode = node;
				}
			}
		}
	}

	private IEnumerator FindPath(Node target) {
		searchCooldown = 3.0f;
		//Debug.Log("Searching for path!");
		List<Node> evaluatedNodes = new List<Node>();
		List<Node> nodesToEvaluate = new List<Node>();
		nodesToEvaluate.Add(currentNode);
		//Debug.Log("Start node: "+currentNode.XID+" "+currentNode.ZID);
		//Debug.Log("Neighbors: "+currentNode.Neighbors.Count);
		foreach(Node node in currentNode.Neighbors) {
			if(node != null) {
				nodesToEvaluate.Add(node);
				node.Parent = currentNode;
				node.GCost = node.CheckDiagonal(currentNode) ? 14 : 10;
			}
			else{
				//Debug.Log("Null");
			}
		}
		evaluatedNodes.Add(currentNode);
		nodesToEvaluate.Remove(currentNode);
		bool stopLoop = false;
		while(nodesToEvaluate.Count > 0 && !stopLoop) {
			Node nodeToEval = null;
			foreach(Node node in nodesToEvaluate) {
				if(nodeToEval == null) {
					nodeToEval = node;
				}
				else if(node.HCost(target) < nodeToEval.HCost(target)) {
					nodeToEval = node;
				}
			}
			//Debug.Log("Node being evaluated: "+nodeToEval.XID+" "+nodeToEval.ZID);
			//player.transform.position = new Vector3(nodeToEval.XCenter, player.transform.position.y, nodeToEval.ZCenter);
			//yield return new WaitForSeconds(Time.deltaTime*10);
			nodesToEvaluate.Remove(nodeToEval);
			evaluatedNodes.Add(nodeToEval);
			if(nodeToEval == target) {
				//Debug.Log("Path to final node found!");
				stopLoop = true;
				CreateNodePath(nodeToEval);
			}
			else {
				foreach(Node node in nodeToEval.Neighbors) {
					if(!evaluatedNodes.Contains(node)) {
						if(node != null) {
							if(!nodesToEvaluate.Contains(node)) {
								nodesToEvaluate.Add(node);
								node.Parent = nodeToEval;
								node.GCost = (node.CheckDiagonal(nodeToEval) ? 14 : 10) + node.Parent.GCost;
							}
							else if(node.GCost < nodeToEval.GCost + (node.CheckDiagonal(nodeToEval) ? 14 : 10)) {
								node.Parent = nodeToEval;
								node.GCost = (node.CheckDiagonal(nodeToEval) ? 14 : 10) + node.Parent.GCost;
							}
						}
						else {
							//Debug.Log("Null");
						}
					}
				}
			}
		}
		//Debug.Log("Pathfinding complete!");
		yield break;
	}


	private void CreateNodePath(Node node) {
		List<Node> path = new List<Node>();
		while(node != null) {
			path.Add(node);
			node = node.Parent;
		}
		path.Reverse();
		currentPath = path;
		foreach(Node node2 in currentPath) {
			//Debug.Log("X: "+node2.XID+" Z: "+node2.ZID);
		}
	}

	private void NewBehavior() {
		if(insanity.CurrentValue + 1 > insanity.maxValue) {
			behavior = Behavior.SUPERAGGRESSIVE;
			//Debug.Log("Monster now superagressive!");
		}
		else{
			if(Random.value > insanity.CurrentValue * 2 / insanity.maxValue) {
				behavior = Behavior.PASSIVE;
				//Debug.Log("Monster now passive!");
			}
			else if(Random.value > insanity.CurrentValue / insanity.maxValue) {
				behavior = Behavior.STALKING;
				//Debug.Log("Monster now stalking!");
			}
			else {
				behavior = Behavior.AGGRESSIVE;
				//Debug.Log("Monster now agressive!");
			}
		}
	}

	private bool CheckForPlayer() {
		// Forward direction vector from camera
		//Vector3 fwd = transform.TransformDirection (Vector3.forward);
		// Direction vector towards monster
		//Vector3 dir = player.transform.position - transform.position;
		//Debug.Log(Vector3.Dot (fwd, dir));
		return common.CheckRange(player.gameObject, "Player", gameObject, 50);

	}

	private bool PlayerLookingAtMonster() {
		float angle = Vector3.Angle(player.transform.forward, transform.forward);
		return angle > 135.0f;
	}

	private void MoveOrder() {
		if(behavior == Behavior.SUPERAGGRESSIVE) {
			MoveToPlayer();
		}
		if(behavior == Behavior.AGGRESSIVE) {
			if(CheckForPlayer()) {
				MoveToPlayer();
			}
			else {
				SearchForPlayer();
			}
		}
		if(behavior == Behavior.STALKING) {
			if(CheckForPlayer()) {
				if(PlayerLookingAtMonster()) {
					MoveAwayFromPlayer();
				}
				else {
					MoveToPlayer();
				}
			}
			else {
				SearchForPlayer();
			}
		}
		if(behavior == Behavior.PASSIVE) {
			if(CheckForPlayer()) {
				MoveAwayFromPlayer();
			}
			else {
				SearchForPlayer();
			}
		}
	}

	private void MoveToPlayer() {
		if(currentPath.Count > 1) {
			//Debug.Log("Monster moving to next node!");
			FindClosestNode();
			if(currentNode == currentPath[0]) {
				SetMoveTarget(currentPath[1]);
				currentPath.RemoveAt(0);
			}
		}
		else {
			//Debug.Log("Monster chasing player!");
			FindClosestNode();
			player.FindClosestNode();
			if(currentNode != player.CurrentNode) {
				if(searchCooldown <= 0.0f) {
					StartCoroutine(FindPath(player.CurrentNode));
				}
				else {
					searchCooldown -= Time.deltaTime;
				}
			}
		}
	}

	private void MoveAwayFromPlayer() {
		//Debug.Log("Monster moving away from player!");
		FindClosestNode();
		player.FindClosestNode();
		Node targetNode = null;
		foreach(Node node in currentNode.Neighbors) {
			if(targetNode == null) {
				targetNode = node;
			}
			else if(node.HCost(player.CurrentNode) > targetNode.HCost(player.CurrentNode)) {
				targetNode = node;
			}
		}
		if(targetNode != null) {
			SetMoveTarget(targetNode);
		}
	}

	private void SearchForPlayer() {
		//Debug.Log("Monster searching for player!");
		FindClosestNode();
		if(currentNode.Neighbors.Count > 0) {
			int node = Random.Range(0, currentNode.Neighbors.Count - 1);
			SetMoveTarget(currentNode.Neighbors[node]);
		}
	}

	private void SetMoveTarget(Node node) {
		if(node.AtPortal && currentNode.AtPortal) {
			//Debug.Log ("Moving through portal!");
			MoveThroughPortal(node);
		}
		else {
			SetMoveTarget(node.XCenter, node.ZID);
		}
	}

	private void SetMoveTarget(float x, float y) {
		moveTarget = true;
		targetX = x;
		targetZ = y;
	}

	private bool ProximityCheck() {
		return Common.Hyp(targetX, transform.position.x, targetZ, transform.position.z) < speed;
	}

	private void MoveThroughPortal(Node node) {
		GameObject[] portals = GameObject.FindGameObjectsWithTag("Mirror");
		GameObject targetPortal = null;
		GameObject targetCamera = null;
		foreach(GameObject portal in portals) {
			if(targetPortal == null) {
				targetPortal = portal;
			}
			else if(Common.Hyp(portal.transform.position.x, transform.position.x, portal.transform.position.z, transform.position.z) <
			        Common.Hyp(targetPortal.transform.position.x, transform.position.x, targetPortal.transform.position.z, transform.position.z)) {
				targetPortal = portal;
			}
			var cameraArray = portal.transform.Cast<Transform>().Where(c=>c.gameObject.tag == "PortalCam").ToArray();
			if(targetCamera == null) {
				targetCamera = cameraArray[0].gameObject;
			}
			else if(Common.Hyp(cameraArray[0].transform.position.x, transform.position.x, cameraArray[0].transform.position.z, transform.position.z) <
			        Common.Hyp(targetCamera.transform.position.x, transform.position.x, targetCamera.transform.position.z, transform.position.z)) {
				targetCamera = cameraArray[0].gameObject;
			}
		}

		if(targetPortal != null && targetCamera != null) {
			if(node.Distance(targetPortal.transform.position.x, targetPortal.transform.position.z) < 
			   node.Distance(targetCamera.transform.position.x, targetCamera.transform.position.z)) {
				portalTarget = true;
				SetMoveTarget(targetPortal.transform.position.x, targetPortal.transform.position.z);
				currPortal = targetPortal;
				//Debug.Log("Portal move target set!");
			}
			else {
				cameraTarget = true;
				SetMoveTarget(targetCamera.transform.position.x, targetCamera.transform.position.z);
				currCamera = targetCamera;
				//Debug.Log("Portal camera move target set!");
			}
		}
	}

}

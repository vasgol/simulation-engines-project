﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Node {

	public int XID { set; get; }
	public int ZID { set; get; }
	public float XCenter { set; get; }
	public float ZCenter { set; get; }
	public List<Node> Neighbors { set; get; }
	public float GCost { set; get; }	
	public Node Parent { set; get; }
	public bool AtPortal { set; get; }
	public bool Entrance { set; get; }

	public Node(int xID, int zID, float xCenter, float zCenter) {
		XID = xID;
		ZID = zID;
		XCenter = xCenter;
		ZCenter = zCenter;
		Neighbors = new List<Node>();
	}

	public Node() {

	}

	public float Distance(float x, float z) {
		return Common.Hyp(XCenter, x, ZCenter, z);
	}

	public bool CheckDiagonal(Node node) {
		return Mathf.Abs(XID - node.XID) == 1 && Mathf.Abs(ZID - node.ZID) == 1;
	}

	public float HCost(Node target) {
		return Distance(target.XCenter, target.ZCenter);
	}

	public float FCost(Node target) {
		return GCost + HCost(target);
	}
	
}

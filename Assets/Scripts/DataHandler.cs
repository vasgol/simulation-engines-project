﻿using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class DataHandler : MonoBehaviour {

	public NodeBuilder builder;
	public Monster monster;
	public Player player;

	// Use this for initialization
	void Start() {
	
	}

	// Update is called once per frame
	void Update() {
	
	}

	public void BuildNodeFile(List<Node> nodeList) {
		//Debug.Log("Building!");
		using (StreamWriter outfile = new StreamWriter(Application.dataPath+"/Nodes.json")) {
			outfile.Write(JsonConvert.SerializeObject(nodeList, Formatting.Indented, new JsonSerializerSettings
			{PreserveReferencesHandling = PreserveReferencesHandling.Objects, NullValueHandling = NullValueHandling.Ignore}));
		}
		//Debug.Log("Built!");
	}

	public void LoadNodeFile() {
		//Debug.Log("Loading!");
		List<Node> nodes;
		using (StreamReader infile = new StreamReader(Application.dataPath+"/Nodes.json")) {
			nodes = JsonConvert.DeserializeObject<List<Node>>(infile.ReadToEnd(), new JsonSerializerSettings
			{Formatting = Formatting.Indented, PreserveReferencesHandling = PreserveReferencesHandling.Objects
				, NullValueHandling = NullValueHandling.Ignore});
		}
		monster.NodeList = nodes;
		player.NodeList = nodes;
		//Debug.Log("Loaded!");
	}

}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FPSCounter : MonoBehaviour {

	private List<float> snapshots = new List<float>();

	// Use this for initialization
	void Start() {
	
	}
	
	// Update is called once per frame
	void Update() {
		snapshots.Add(1.0f/Time.deltaTime);
		if(snapshots.Count > 20)
			snapshots.RemoveAt(0);
		float fpsEstimate = 0;
		foreach(float estimate in snapshots)
			fpsEstimate += estimate;
		fpsEstimate /= (float)(snapshots.Count);
		gameObject.guiText.text = "FPS: "+fpsEstimate.ToString();
	}
}

﻿using UnityEngine;
using System.Collections;

public class Common : MonoBehaviour {


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// Checks if an Object is is visible from a camera 
	public bool CheckVisible(GameObject obj, string objtag, GameObject origin, int raylength) {
		RaycastHit hit;

		// Forward direction vector from camera
		Vector3 fwd = origin.transform.TransformDirection (Vector3.forward);
		// Direction vector towards monster
		Vector3 dir = obj.transform.position - origin.transform.position;

		// Cast a ray towards monster.
		// If we get a hit, and the object is visible to the camera and it is not the wall
		// Then increase amount of radial blurring (scared player)			
		return Physics.Raycast (origin.transform.position, dir, out hit, raylength) && Vector3.Dot (fwd, dir) > 4
		        && hit.collider.tag == objtag;
	}

	public bool CheckRange(GameObject obj, string objtag, GameObject origin, int raylength) {
		RaycastHit hit;
		
		// Forward direction vector from camera
		Vector3 fwd = origin.transform.TransformDirection (Vector3.forward);
		// Direction vector towards monster
		Vector3 dir = obj.transform.position - origin.transform.position;
		
		// Cast a ray towards monster.
		// If we get a hit, and the object is visible to the camera and it is not the wall
		// Then increase amount of radial blurring (scared player)			
		return Physics.Raycast (origin.transform.position, dir, out hit, raylength)
			&& hit.collider.tag == objtag;
	}

	public static float Hyp(float x, float x2, float z, float z2) {
		return Mathf.Sqrt(Mathf.Pow(Mathf.Abs(x - x2), 2) + Mathf.Pow(Mathf.Abs(z - z2), 2));
	}
}

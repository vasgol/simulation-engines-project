﻿using UnityEngine;
using System.Collections;

public class Portal_camera : MonoBehaviour{


	private GameObject maincam;
	private	GameObject mirror;
	private GameObject monster;
	private Graphics graphics;
	private Common common;
	private string parentName;
	private bool mirrorvisible = false;
	Vector3 distance = new Vector3(0,0,0);

	void Start() {

		parentName = this.transform.parent.name;
		mirror = GameObject.Find (parentName);
		maincam = GameObject.FindGameObjectWithTag ("Camera");
		monster = GameObject.FindGameObjectWithTag ("Monster");
		graphics = (Graphics)this.GetComponent("Graphics");
		common = (Common) GameObject.FindGameObjectWithTag ("Common").GetComponent("Common");
	}
	
	void Update()
	{
		mirrorvisible = common.CheckVisible (mirror, "Mirror", maincam.gameObject, 50);
		
		graphics.portal_mirror (mirrorvisible, mirror);

		//if (mirrorvisible){
		//	distance.x = mirror.transform.position.x - maincam.transform.position.x;
		//	this.transform.position -= distance;
		//}
		
	} 
}

using UnityEngine;
using System.Collections;

public class Audio : MonoBehaviour {
	
	private GameObject monster;
	private GameObject mainCamera;

	
	private Common common;

	//Player audio variables
	private AudioSource portalAudioSource;
	private GameObject portalAudio;
	private float originalVolume;
	private float audioTimer = 7.5f;
	private bool audioPause = false;

	//Monster audio variables
	private GameObject monsterSound;
	private GameObject portalAudioMonster;
	private AudioSource portalAudioSourceMonster;
	private float monsterOriginalVolume;
	private float monsterAudioTimer = 7.5f;
	private bool monsterAudioPause = false;

	// Use this for initialization
	void Start () {
		
		monster = GameObject.FindGameObjectWithTag ("Monster");
		mainCamera = GameObject.FindGameObjectWithTag ("Camera");
		monsterSound = GameObject.Find ("monstersound");
		common = (Common) GameObject.FindGameObjectWithTag ("Common").GetComponent("Common");
		
		originalVolume = monster.audio.volume;
		monsterOriginalVolume = monsterSound.audio.volume;
		
		//If the player sees the monster through a portal
		portalAudio = new GameObject ("Portal Audio");
		portalAudioSource = portalAudio.AddComponent<AudioSource>();
		portalAudioSource.rolloffMode = monster.audio.rolloffMode;
		portalAudioSource.clip = monster.audio.clip;
		
		//If the monster sees the player through a portal
		portalAudioMonster = new GameObject ("Portal Audio Monster");
		portalAudioSourceMonster = portalAudioMonster.AddComponent<AudioSource>();
		portalAudioSourceMonster.rolloffMode = monsterSound.audio.rolloffMode;
		portalAudioSourceMonster.clip = monsterSound.audio.clip;
	}
	
	// Update is called once per frame
	void Update () {

		if(audioTimer > 0 && audioPause)
			audioTimer -= Time.deltaTime;
		if(audioTimer <= 0){
			audioPause = false;
			audioTimer = 7.5f;
		}
		
		if(monsterAudioTimer > 0 && monsterAudioPause)
			monsterAudioTimer -= Time.deltaTime;
		if(monsterAudioTimer <= 0){
			monsterAudioPause = false;
			monsterAudioTimer = 7.5f;
		}

	}
	
	public void playerSeesMonsterAudio(bool playerSeesMonsterVariable, bool playerSeesThroughMirror, GameObject portalCamera, GameObject portal) {
		

		if (playerSeesMonsterVariable && !playerSeesThroughMirror && !audioPause && !monsterAudioPause) {
			monster.audio.volume = originalVolume;
			playerSeesMonster ();
		}
		else if(!playerSeesMonsterVariable && playerSeesThroughMirror && !audioPause && !monsterAudioPause) {
			portalAudioSource.volume = originalVolume;
			playerSeesMirror(portalCamera, portal);
		}
		else if(playerSeesMonsterVariable && playerSeesThroughMirror && !audioPause && !monsterAudioPause) {

			float normalDistance = Vector3.Distance (monster.transform.position, mainCamera.transform.position);
			float euclideanDistance = Vector3.Distance (monster.transform.position, portalCamera.transform.position) 
				+ Vector3.Distance (portal.transform.position, mainCamera.transform.position);
			
			float percentage = normalDistance / (normalDistance + euclideanDistance);
			
			monster.audio.volume = (1.0f - percentage) * monsterOriginalVolume;
			portalAudioSource.audio.volume = percentage * monsterOriginalVolume;
			
			playerSeesMonster();
			playerSeesMirror(portalCamera, portal);

		}
	}
	
	public void playerSeesMonster(){
		if(!monster.audio.isPlaying){
			monster.audio.Play ();

			audioPause = true;
		}
	}
	
	public void playerSeesMirror(GameObject origin, GameObject mirror){
		if(!portalAudioSource.isPlaying ){
			
			//Set position of Audio coming from portal
			Vector3 dis = monster.transform.position - origin.transform.position;
			float portalAngle = Vector3.Angle(mirror.transform.forward, origin.transform.forward);
			dis = Quaternion.Euler(0, -portalAngle, 0) * dis;
			portalAudio.transform.position = mirror.transform.position + dis;
			
			portalAudioSource.Play();

			audioPause = true;
		}
	}
	
	//Handles what happens if the monster sees the player
	public void monsterSeesPlayerAudio(bool monsterSeesPlayerVariable, bool monsterSeesThroughMirror, GameObject portalCamera, GameObject portal){
		

		if (monsterSeesPlayerVariable && !monsterSeesThroughMirror && !monsterAudioPause) {
			monsterSound.audio.volume = monsterOriginalVolume;
			monsterSeesPlayer();
		} 
		else if (!monsterSeesPlayerVariable && monsterSeesThroughMirror && !monsterAudioPause) {
			portalAudioSourceMonster.audio.volume = monsterOriginalVolume;
			monsterSeesMirror (portalCamera, portal);
		}
		else if (monsterSeesPlayerVariable && monsterSeesThroughMirror && !monsterAudioPause){
			float normalDistance = Vector3.Distance (monster.transform.position, mainCamera.transform.position);
			float euclideanDistance = Vector3.Distance (monster.transform.position, portal.transform.position) 
				+ Vector3.Distance (portal.transform.position, mainCamera.transform.position);
			
			float percentage = normalDistance / (normalDistance + euclideanDistance);

			
			monsterSound.audio.volume = (1.0f - percentage) * monsterOriginalVolume;
			portalAudioSourceMonster.audio.volume = percentage * monsterOriginalVolume;
			
			monsterSeesPlayer();
			monsterSeesMirror(portalCamera, portal);
			
		}
		
	}
	
	public void monsterSeesPlayer(){
		

		if (!monsterSound.audio.isPlaying){
			monsterSound.audio.Play ();
			monsterAudioPause = true;
		}

	}
	
	public void monsterSeesMirror(GameObject origin, GameObject mirror){
		
		
		if(!portalAudioSourceMonster.audio.isPlaying ){

			//Set position of Audio coming from portal
			Vector3 dis = monster.transform.position - mirror.transform.position;
			float portalAngle = Vector3.Angle(mirror.transform.forward, origin.transform.forward);
			dis = Quaternion.Euler(0, portalAngle, 0) * dis;
			portalAudioMonster.transform.position = origin.transform.position + dis;
			
			portalAudioSourceMonster.Play();

			monsterAudioPause = true;
		}
	}
	
	
}

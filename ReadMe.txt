﻿Hyper Reality Tech Demo

Press the clone button and paste the command into Git-Bash to clone the repository.

Simulation that provides navigation in a non-Euclidean maze with a first person controller and a AI monster. 
By default, sound effects, artificial intelligence and radial blurring effect are disabled. 


Open the project in Unity and run it. 


Hotkeys



* Keyboard        
W                Move Forward 
A                Move Left
S                Move Backward
D                Move Right
Space            Jump
L                Activate Monster AI (Loads nodes from JSON file)
R                Build nodes
B                Save nodes to JSON file
I                Toggle Radial Blurring
O                Toggle Sound Effects
P                Toggle Ambient Background
* Mouse         
Padding          Look right/left


Graphics:


* Glowing sources 
   * Glow amount value can be adjusted through the inspector with a slider through the inspector to the objects “Sign”, “Skull” and “Monster”.
   * During runtime, this value is adjusted depending on the insanity meter. As insanity rises, objects glowing amount changes with a faster pace.


* Screen Distortion (Post-Process)
   * This effect is visible, when the player collides with the monster, giving the feeling of getting hit.


* Radial Blurring (Post-Process)
   * Press key “I” to activate. This effect is visible when player watches the monster either directly, 
     or through the non-Euclidean portals, giving the feeling of frustration. 
     It slowly fades out when player is facing away from the monster. 

Portal Rendering:

* Portals are rendered only if they are visible to the player. 


AI:

* Node Building
   * You can press R to build the node data from the terrain. Note that this will take quite a long time to complete.
* Node Saving
   * You can press B to save the node data to a JSON file. To do this, you must first have built the data using R.
* Node Loading
   * Instead of building the node data each time which takes a long time, you can also node it from the previous JSON file by pressing L. 
     Note that the game will freeze for a short time while it loads. After the nodes are loaded, the monster AI will automatically activate.
       
Audio:
* Monster sound effects, press O to enable/disable. 
   * When the monster sees you, it will emit a high screech
   * When you see the monster, it will emit a low growl
   * The sound effects has a 7.5 second cooldown to reduce spamming of the effects. 
     The screech has higher priority than the growl, when the screech is activated the growl can not be activated for 7.5 seconds,
     but when the growl is activated, the screech can still be activated as well. 
   * Ambient background music, press P to enable/disable.
